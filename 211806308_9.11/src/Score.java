import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.swing.text.StyleContext.SmallAttributeSet;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Score {

	public static void main(String[] args) {
		// 加载配置文件
		Map<String, Double> totalScore = getTotalScore("total.properties");
		// 初始化自己成绩的哈希表
		// 如果输入的两个源文件的参数不足，提示用户并结束程序
		/*if(args.length < 2) {
			System.out.println("输入参数缺少！\n参数1 - 小班课的网页源文件\n参数2 - 大班课的网页源文件。");
			System.exit(0);
		}*///这里运用于git bash无传参时的报错
		//Map<String, Double> iScore = initIScore(args[0], args[1]);//此传参方式仅限于git bash运行
		Map<String, Double> iScore = initIScore("small.html", "all.html");
		// 计算并输出结果
		System.out.println(String.format("%.2f", calFinalScore(totalScore, iScore)));

	}
	
	/**
	 * @param totalScore 各类经验的上限
	 * @param iScore 自己获得的经验
	 * @return 根据公式计算出的最终分数
	 */
	//分数计算模块
	public static double calFinalScore(Map<String, Double> totalScore, Map<String, Double> iScore) {
		// 课前自测部分换算成百分制
		iScore.put("before", iScore.get("before") / totalScore.get("before") * 100);
		// 课堂完成部分换算成百分制并 95 折
		iScore.put("base", iScore.get("base") / totalScore.get("base") * 95);
		// 课堂小测部分换算成百分制
		iScore.put("test", iScore.get("test") / totalScore.get("test") * 100);
		// 编程题部分换算成百分制若超过 95 则为 95
		iScore.put("program", iScore.get("program") / totalScore.get("program") * 100);
		if(iScore.get("program") > 95) {
			iScore.put("program", 95.00);
		}
		// 附加题部分换算成百分制若超过 90 则为 90
		iScore.put("program", iScore.get("program") / totalScore.get("program") * 100);
		if(iScore.get("program") > 90) {
			iScore.put("program", 90.00);
		}
		// 计算总分
		double finalScore;
		finalScore = iScore.get("before") * 0.25 + iScore.get("base") * 0.3 + iScore.get("test") * 0.2 + iScore.get("program") * 0.1 + iScore.get("add") * 0.05;
		return finalScore;
	}
	/**
	 * @param filenameSmall small.html 的文件路径
	 * @param filenameAll all.html 的文件路径
	 * @return 自己获得的经验值
	 */
	//计算自己获得的分数
	public static Map<String, Double> initIScore(String filenameSmall, String filenameAll) {
		Map<String, Double> iScore = new HashMap<String, Double>();
		iScore.put("before",0.0);
		iScore.put("base",0.0);
		iScore.put("test",0.0);
		iScore.put("program",0.0);
		iScore.put("add",0.0);
		try {
			// 读取 small 与 all 源文件
			Document small = Jsoup.parse(new File(filenameSmall), "utf-8");
			Document all = Jsoup.parse(new File(filenameAll), "utf-8");
			// 获得 small 中所有的活动代码块
			Elements activities = small.getElementsByClass("interaction-row");
			activities.addAll(all.getElementsByClass("interaction-row"));
			
			for (int i = 0; i < activities.size(); i++) {
				// 判断是属于什么类型的活动和完成情况
				if (activities.get(i).getElementsByClass("interaction-name").get(0).toString().contains("课堂完成")) {
					// 获取课堂完成部分且有参与的经验
					iScore.put("base", getScore(activities.get(i)) + iScore.get("base"));
				} else if (activities.get(i).getElementsByClass("interaction-name").get(0).toString().contains("课堂小测")) {
					// 获取课堂小测部分且有参与的经验
					iScore.put("test", getScore(activities.get(i)) + iScore.get("test"));
				} else if (activities.get(i).getElementsByClass("interaction-name").get(0).toString().contains("编程题")) {
					// 获取编程题部分且有参与的经验
					iScore.put("program", getScore(activities.get(i)) + iScore.get("program"));
				} else if (activities.get(i).getElementsByClass("interaction-name").get(0).toString().contains("附加题")) {
					// 获取附加题部分且有参与的经验
					iScore.put("add", getScore(activities.get(i)) + iScore.get("add"));
				} else if (activities.get(i).getElementsByClass("interaction-name").get(0).toString().contains("课前自测")) {
					// 获取课前自测部分且有参与的经验
					iScore.put("before", getScore(activities.get(i)) + iScore.get("before"));
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("查找文件失败，路径上没有该文件，可能是路径写错！");
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("文件读取异常！");
		}
		return iScore;
	}
	
	/**
	 * @param e 需要解析的活动代码段
	 * @return 解析之后的提取出的经验
	 */
	//计算自己获取的经验
	public static double getScore(Element e) {
		// 将一个活动中的所有 span 标签提取出来
		Elements span = e.select("span[style]");
		double score = 0;
		// 提取经验
		for(int i = 0; i < span.size(); i++) {
			// 判断是否含有经验关键字
			if(span.get(i).text().contains("经验")) {       // 表示分离出来的第 i 个标签的文本内容含有 “经验” 关键字
				// 含有 “互评” 和不含有的计算方式不一样，先按空格分割
				if(span.get(i).text().contains("互评")) {   // 表示分离出来的第 i 个标签的文本内容含有 “互评” 关键字
					// 互评 x 经验
					// 如果是 “互评” ，[1] 即为经验数字的下标
					score += Double.parseDouble(span.get(i).text().split(" ")[1]);
				}else {
					// x 经验
					// 如果不是 “互评”，[0] 即为经验数字的下标
					score += Double.parseDouble(span.get(i).text().split(" ")[0]);
				}
			}
		}
		return score;
	}
	
	/**
	 * @param filename 配置文件的路径
	 * @return 各类经验的上限
	 */
	//获取总分数
	public static Map<String, Double> getTotalScore(String filename) {
		Properties pp = new Properties();
		Map<String, Double> totalScore = new HashMap<String, Double>();
		try {
			pp.load(new FileInputStream(filename));
			totalScore.put("before", Double.parseDouble(pp.getProperty("before")));
			totalScore.put("base", Double.parseDouble(pp.getProperty("base")));
			totalScore.put("test", Double.parseDouble(pp.getProperty("test")));
			totalScore.put("program", Double.parseDouble(pp.getProperty("program")));
			totalScore.put("add", Double.parseDouble(pp.getProperty("add")));
		} catch (FileNotFoundException e1) {
			System.out.println("没找到配置文件！");
		} catch (IOException e1) {
			System.out.println("配置文件读取失败！");
		}
		return totalScore;
	}
}